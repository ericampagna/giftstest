<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Question;
use App\Category;
use App\Ministry;

class TestController extends Controller
{
    public function index()
    {
    	
    	$questions = Question::all();
    	$ministries = Ministry::all();

    	return view('welcome')->with(['questions' => $questions, 'ministries' => $ministries]);
    }

    public function submitTest(Request $request)
    {
    	$categories = Category::all();
    	$ministries = Ministry::all();
    	$score = array();
    	foreach($categories as $cat)
    	{
    		$cat->score = 0;

    		$catQuestions = $cat->questions;
    		//dd($cat);
    		foreach($catQuestions as $cq)
    		{
    			$cq_score = $request->input('q_'.$cq->id);
    			$cat->score = $cat->score + $cq_score;
    		}
    		$score['Gifts'][$cat->name] = $cat->score;

    		foreach($ministries as $m)
	    	{
	    		$score['Passions'][$m->name] = $request->input('p_'.$m->id);

	    		$score['Total'][$m->name.'('.$cat->name.')'] = ((15.625 * $score['Gifts'][$cat->name]) * $request->input('p_'.$m->id))/10;
	    		$score['Breakdown'][$m->name][$cat->name] = ((15.625 * $score['Gifts'][$cat->name]) * $request->input('p_'.$m->id))/10;

	    	}

	    	// $score['Max'] = max(
	    	// 	$score['Total']['Outreach'],
	    	// 	$score['Total']['Front Door'],
	    	// 	$score['Total']['Tech'],
	    	// 	$score['Total']['Worship'],
	    	// 	$score['Total']['Connections'],
	    	// 	$score['Total']['Community Kids'],
	    	// 	$score['Total']['Community Students'],
	    	// 	$score['Total']['Prayer Ministry']
	    	// 	);
	    	arsort($score['Total']);
	    	$score['Top3'] = array_slice($score['Total'], 0, 3, true);
	    	// //dd($top3);
	    	// foreach($top3 as $key => $value) {
	    	// 	$score['Top3'][] = $key .' ' .$value;
	    	// }
	    	$score['Max'] = array_search(max($score['Total']), $score['Total']);
    		
    	}

    	

    	return view('score')->with(['score' => $score]);

    }
}
