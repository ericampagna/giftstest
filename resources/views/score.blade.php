<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" value="{{ csrf_token() }}" id="token" />
        <link rel="stylesheet" type="text/css" href="css/app.css">
        <script>
            window.Laravel = {!! json_encode([
                'csrfToken' => csrf_token(),
            ]) !!};
        </script>
        <title>Spirital Gifts Test</title>
    </head>
    <body >
    <div class="container">
        <h1>Spirital Gifts Test</h1>
        <div class="row">
            <h1>Your Score</h1>
            @foreach($score['Top3'] as $topname => $num)
              <h4>{{$topname}}: {{$num}}</h4>
            @endforeach

            <h3>Breakdown</h3>
            @foreach($score['Breakdown'] as $minname => $min)
              <h5>{{$minname}}</h5>
              <ul>
              @foreach($min as $catname => $cat)
                <li><b>{{$catname}}: </b>{{$cat}}</li>
              @endforeach
              </ul>
            @endforeach

            <h3>Gifts</h3>
            <ul>
            @foreach($score['Gifts'] as $name => $num)
              <li><b>{{$name}}: </b>{{$num}}</li>
            @endforeach
            </ul>

            <h3>Passions</h3>
            <ul>
            @foreach($score['Passions'] as $name => $num)
              <li><b>{{$name}}: </b>{{$num}}</li>
            @endforeach
            </ul>
        </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <script type="text/javascript" src="js/app.js"></script>
    </body>
</html>
