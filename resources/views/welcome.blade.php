<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" value="{{ csrf_token() }}" id="token" />
        <link rel="stylesheet" type="text/css" href="css/app.css">
        <script>
            window.Laravel = {!! json_encode([
                'csrfToken' => csrf_token(),
            ]) !!};
        </script>
        <title>Spirital Gifts Test</title>
    </head>
    <body >
    <div class="container">
        <h1>Spirital Gifts Test</h1>
        <div class="row">
            <form class="col s12 TestForm" action="/" method="post">
             {{ csrf_field() }}
                <div class="row">
                <blockquote>
                    <p>Don’t answer based on what you think you should say. Respond with what you really think and feel, based on interest and experience. No modesty or over-inflation either, be honest. If you’re strong in an area or have no interest at all, don’t water it down or sugar-coat it.</p>
                </blockquote>
                    @foreach($questions as $question)
                        <div class="input-field">
                            <h5>{{$question->question}}</h5>
                            <p>
                              <em>
                                <input id="q_{{$question->id}}_0" name="q_{{$question->id}}" value="0" type="radio" >
                                <label for="q_{{$question->id}}_0">Not at all</label>
                              </em>
                              <em>
                                 <input id="q_{{$question->id}}_1" name="q_{{$question->id}}" value="1" type="radio" >
                                <label for="q_{{$question->id}}_1">Little</label>
                              </em>
                              <em>
                                <input id="q_{{$question->id}}_2" name="q_{{$question->id}}" value="2" type="radio" >
                                <label for="q_{{$question->id}}_2">Moderately</label>
                              </em>
                              <em>
                                <input id="q_{{$question->id}}_3" name="q_{{$question->id}}" value="3" type="radio" >
                                <label for="q_{{$question->id}}_3">Considerably</label>
                              </em>
                              <em>
                                <input id="q_{{$question->id}}_4" name="q_{{$question->id}}" value="4" type="radio" >
                                <label for="q_{{$question->id}}_4">Strongly</label>
                              </em>
                            </p>
                        </div>
                    @endforeach
                    <blockquote>
                        <h5>Rate your passion level for the following Ministry Areas:</h5>
                    </blockquote>
                    @foreach($ministries as $ministry)
                        <h4>{{$ministry->name}}</h4>
                        <i>Examples: {{$ministry->examples}}</i>
                        <div class="input-field">
                            <p>Passion Level:</p>
                            <p>
                              <em>
                                <input id="p_{{$ministry->id}}_0" name="p_{{$ministry->id}}" value="0" type="radio" >
                                <label for="p_{{$ministry->id}}_0">Not at all</label>
                              </em>
                              <em>
                                 <input id="p_{{$ministry->id}}_1" name="p_{{$ministry->id}}" value="1" type="radio" >
                                <label for="p_{{$ministry->id}}_1">Somewhat Passionate</label>
                              </em>
                              <em>
                                <input id="p_{{$ministry->id}}_2" name="p_{{$ministry->id}}" value="2" type="radio" >
                                <label for="p_{{$ministry->id}}_2">Moderatley Passionate</label>
                              </em>
                              <em>
                                <input id="p_{{$ministry->id}}" value="3" type="radio" >
                                <label for="p_{{$ministry->id}}_3">Passionate</label>
                              </em>
                              <em>
                                <input id="p_{{$ministry->id}}_4" name="p_{{$ministry->id}}" value="4" type="radio" >
                                <label for="p_{{$ministry->id}}_4">Very Passionate</label>
                              </em>
                            </p>
                        </div>
                         <div class="input-field">
                            <p>Interest Level:</p>
                            <p>
                              <em>
                                <input id="i_{{$ministry->id}}_0" name="i_{{$ministry->id}}" value="none" type="radio" >
                                <label for="i_{{$ministry->id}}_0">None</label>
                              </em>
                              <em>
                                 <input id="i_{{$ministry->id}}_1" name="i_{{$ministry->id}}" value="currently_active" type="radio" >
                                <label for="i_{{$ministry->id}}_1">Currently Active</label>
                              </em>
                              <em>
                                <input id="i_{{$ministry->id}}_2" name="i_{{$ministry->id}}" value="interested" type="radio" >
                                <label for="i_{{$ministry->id}}_2">Interested and would like to know how to get involved</label>
                              </em>
                            </p>
                        </div>
                    @endforeach
                    <div class="input-field">
                    <button class="btn waves-effect waves-light" type="submit" name="action">Submit
                      </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <script type="text/javascript" src="js/app.js"></script>
    </body>
</html>
